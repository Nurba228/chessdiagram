package pieces;

public class Queen extends Piece{
    public Queen(String pieceColor) {
        super(pieceColor);
    }

    @Override
    public void move() {
        System.out.println("Queen move");;
    }
}
