package pieces;

public class Rook extends Piece{
    public Rook(String pieceColor) {
        super(pieceColor);
    }

    @Override
    public void move() {
        System.out.println("Rook move");;
    }
}
