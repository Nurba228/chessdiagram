package pieces;

public class King extends Piece{
    public King(String pieceColor) {
        super(pieceColor);
    }

    @Override
    public void move() {
        System.out.println("King Move");;
    }
}
