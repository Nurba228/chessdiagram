package pieces;

public abstract class Piece {


    private String pieceColor;
    private boolean isKilled;

    public Piece(String pieceColor) {
        this.pieceColor = pieceColor;
    }

    public String getPieceColor() {
        return pieceColor;
    }

    public void setPieceColor(String pieceColor) {
        this.pieceColor = pieceColor;
    }

    public boolean isKilled() {
        return isKilled;
    }

    public void setKilled(boolean killed) {
        isKilled = killed;
    }

    public void move(){
        System.out.println("Some move");
    }
}
