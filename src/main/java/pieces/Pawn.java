package pieces;

public class Pawn extends Piece{

    private Piece promoteTo;

    public Pawn(String pieceColor) {
        super(pieceColor);
    }

    @Override
    public void move() {
        System.out.println("Pawn move");;
    }
}
