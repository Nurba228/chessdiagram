package pieces;

public class Bishop extends Piece {

    public Bishop(String color) {
        super(color);
    }

    @Override
    public void move() {
        System.out.println("Bishop Move");
    }
}
