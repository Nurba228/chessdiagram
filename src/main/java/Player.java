public abstract class Player {

    public boolean whiteSide;
    public boolean humanPlayer;


    public boolean isHumanPlayer() {
        return this.humanPlayer;
    }

    public boolean isWhiteSide() {
        return this.whiteSide;
    }
}
