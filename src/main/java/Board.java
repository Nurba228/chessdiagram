public class Board {

    private Square[][] squares;

    public Square getSquares(int row, int column) throws Exception {

        if (row < 0 || row > 7 || column < 0 || column > 7) {
            throw new Exception("Index out of bound");
        }

        return squares[row][column];
    }




    public void setSquares(Square[][] squares) {
        this.squares = squares;
    }

    public void createBoardAndReset() {
        System.out.println("Created Board");
    }
}
